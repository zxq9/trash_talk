# Trash Talk
This project is a tutorial example service that implements a simple chat service over telnet.

It is explained at length in two videos suitable for beginning and intermediate Erlang learners.

## Videos
- [Erlang: Telnet Chat Server Part 1! Getting the basics out from scratch (cheating with ZX)](https://rumble.com/ve8h9r-erlang-telnet-chat-server-part-1-getting-the-basics-out-from-scratch-cheati.html)
- [Erlang: Telnet Chat Server Part 2! Creating Channels! (aka "more cheating with ZX")](https://rumble.com/ve8ilt-erlang-telnet-chat-server-part-2-creating-channels-aka-more-cheating-with-z.html)
