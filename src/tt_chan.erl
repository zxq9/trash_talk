%%% @doc
%%% Trash Talk: Chan Worker
%%% @end

-module(tt_chan).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Manager Interface
-export([start/3]).
%% Client Interface
-export([join/3, users/1, chat/3, leave/1, topic/2, topic/1, claim/1]).
%% gen_server
-export([start_link/3]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {name  = ""   :: string(),
         topic = ""   :: string(),
         owner = none :: none | pid(),
         users = []   :: [user()]}).

-record(user,
        {name = ""   :: string(),
         pid  = none :: none | pid(),
         mon  = none :: none | reference()}).


-type state() :: #s{}.
-type user()  :: #user{}.



%%% Manager Interface

-spec start(ChanName, OwnerName, OwnerPID) -> {ok, pid()}
    when ChanName  :: string(),
         OwnerName :: string(),
         OwnerPID  :: pid().

start(ChanName, OwnerName, OwnerPID) ->
    tt_chan_sup:start_chan(ChanName, OwnerName, OwnerPID).


%%% Client Interface

-spec join(ChanPID, UserName, UserPID) -> ok
    when ChanPID  :: pid(),
         UserName :: string(),
         UserPID  :: pid().

join(ChanPID, UserName, UserPID) ->
    gen_server:cast(ChanPID, {join, UserName, UserPID}).


-spec users(ChanPID :: pid()) -> [UserName :: string()].

users(ChanPID) ->
    gen_server:call(ChanPID, users).


-spec chat(ChanPID :: pid(), Sender :: string(), Message :: string()) -> ok.

chat(ChanPID, Sender, Message) ->
    gen_server:cast(ChanPID, {chat, Sender, Message, self()}).


-spec leave(ChanPID :: pid()) -> ok.

leave(ChanPID) ->
    gen_server:cast(ChanPID, {leave, self()}).


-spec topic(ChanPID, Message) -> Result
    when ChanPID :: pid(),
         Message :: string(),
         Result  :: ok | {error, not_owner}.

topic(ChanPID, Message) ->
    gen_server:call(ChanPID, {topic, Message, self()}).


-spec topic(ChanPID :: pid()) -> Topic :: string().

topic(ChanPID) ->
    gen_server:call(ChanPID, topic).


-spec claim(ChanPID) -> Result
    when ChanPID :: pid(),
         Result  :: ok | {error, owned}.

claim(ChanPID) ->
    gen_server:call(ChanPID, {claim, self()}).


%%% gen_server

-spec start_link(ChanName, OwnerName, OwnerPID) -> Result
    when ChanName  :: string(),
         OwnerName :: string(),
         OwnerPID  :: pid(),
         Result    :: {ok, pid()}
                    | {error, Reason :: term()}.

start_link(ChanName, OwnerName, OwnerPID) ->
    Args = {ChanName, OwnerName, OwnerPID},
    gen_server:start_link(?MODULE, Args, []).


init({ChanName, OwnerName, OwnerPID}) ->
    OwnerMon = monitor(process, OwnerPID),
    Owner = #user{name = OwnerName, pid = OwnerPID, mon = OwnerMon},
    State = #s{name = ChanName, owner = OwnerPID, users = [Owner]},
    {ok, State}.


handle_call(users, _, State) ->
    Result = do_users(State),
    {reply, Result, State};
handle_call({topic, Message, RequestorPID}, _, State) ->
    {Result, NewState} = do_topic(Message, RequestorPID, State),
    {reply, Result, NewState};
handle_call(topic, _, State = #s{topic = Topic}) ->
    {reply, Topic, State};
handle_call({claim, UserPID}, _, State) ->
    {Result, NewState} = do_claim(UserPID, State),
    {reply, Result, NewState};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp", [From, Unexpected]),
    {noreply, State}.


handle_cast({join, UserName, UserPID}, State) ->
    NewState = do_join(UserName, UserPID, State),
    {noreply, NewState};
handle_cast({chat, SenderName, Message, SenderPID}, State) ->
    ok = do_chat(SenderName, Message, SenderPID, State),
    {noreply, State};
handle_cast({leave, UserPID}, State) ->
    NewState = do_leave(UserPID, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp", [Unexpected]),
    {noreply, State}.


handle_info({'DOWN', Mon, process, PID, Reason}, State) ->
    NewState = handle_down(Mon, PID, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
    {noreply, State}.


handle_down(Mon, PID, Reason, State = #s{name = ChanName, owner = OwnerPID, users = Users}) ->
    case lists:keytake(PID, #user.pid, Users) of
        {value, #user{}, []} ->
            exit(normal);
        {value, #user{name = UserName, pid = OwnerPID}, NewUsers} ->
            Notice = [UserName, " (owner) left #", ChanName],
            ok = notify(Notice, Users, ChanName),
            State#s{owner = none, users = NewUsers};
        {value, #user{name = UserName}, NewUsers} ->
            Notice = [UserName, " left #", ChanName],
            ok = notify(Notice, Users, ChanName),
            State#s{users = NewUsers};
        false ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
            State
    end.



-spec code_change(OldVersion, State, Extra) -> {ok, NewState} | {error, Reason}
    when OldVersion :: Version | {old, Version},
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         NewState   :: term(),
         Reason     :: term().

code_change(_, State, _) ->
    {ok, State}.


terminate(_, _) ->
    ok.



%%% Doer Functions

do_users(#s{users = Users}) ->
    [Name || #user{name = Name} <- Users].


do_join(UserName, UserPID, State = #s{name = ChanName, users = Users}) ->
    case lists:keymember(UserPID, #user.pid, Users) of
        false ->
            UserMon = monitor(process, UserPID),
            User = #user{name = UserName, pid = UserPID, mon = UserMon},
            Notice = [UserName, " joined #", ChanName],
            ok = notify(Notice, Users, ChanName),
            State#s{users = [User | Users]};
        true ->
            State
    end.


do_chat(SenderName, Message, SenderPID, #s{name = ChanName, users = Users}) ->
    Send =
        fun(#user{pid = PID}) ->
            case PID == SenderPID of
                false -> tt_client:chat(PID, ChanName, SenderName, Message);
                true  -> ok
            end
        end,
    lists:foreach(Send, Users).


do_leave(UserPID, State = #s{name = ChanName, owner = OwnerPID, users = Users}) ->
    case lists:keytake(UserPID, #user.pid, Users) of
        {value, #user{}, []} ->
            exit(normal);
        {value, #user{name = UserName, pid = OwnerPID, mon = Mon}, NewUsers} ->
            true = demonitor(Mon, [flush]),
            Notice = [UserName, " (owner) left #", ChanName],
            ok = notify(Notice, Users, ChanName),
            State#s{owner = none, users = NewUsers};
        {value, #user{name = UserName, mon = Mon}, NewUsers} ->
            true = demonitor(Mon, [flush]),
            Notice = [UserName, " left #", ChanName],
            ok = notify(Notice, Users, ChanName),
            State#s{users = NewUsers};
        false ->
            State
    end.


do_topic(Message, RequestorPID, State = #s{owner = RequestorPID}) ->
    do_topic2(Message, RequestorPID, State);
do_topic(Message, RequestorPID, State = #s{owner = none}) ->
    do_topic2(Message, RequestorPID, State);
do_topic(_, _, State) ->
    {{error, not_owner}, State}.

do_topic2(Message, RequestorPID, State = #s{name = ChanName, users = Users}) ->
    ok = tt_chan_man:topic(ChanName, Message),
    #user{name = UserName} = lists:keyfind(RequestorPID, #user.pid, Users),
    Notice = [UserName, " set topic to \"", Message, "\""],
    ok = notify(Notice, Users, ChanName),
    {ok, State#s{topic = Message}}.


do_claim(UserPID, State = #s{name = ChanName, owner = none, users = Users}) ->
    case lists:keyfind(UserPID, #user.pid, Users) of
        #user{name = UserName} ->
            Notice = [UserName, " claimed ownership of #", ChanName],
            ok = notify(Notice, Users, ChanName),
            {ok, State#s{owner = UserPID}};
        false ->
            {{error, bad_user}, State}
    end;
do_claim(_, State) ->
    {{error, owned}, State}.


notify(Message, Users, ChanName) ->
    Notify = fun(#user{pid = PID}) -> tt_client:notice(PID, ChanName, Message) end,
    lists:foreach(Notify, Users).
