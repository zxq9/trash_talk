%%% @doc
%%% Trash Talk: Chan Worker Manager
%%% @end

-module(tt_chan_man).
-vsn("0.1.0").
-behavior(gen_server).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

%% Service interface
-export([join/2, list/0, users/1, topic/2]).
%% gen_server
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions

-record(chan,
        {name  = ""   :: string(),
         topic = ""   :: string(),
         pid   = none :: none | pid(),
         mon   = none :: none | reference()}).

-record(s,
        {chans = [] :: [chan()]}).


-type state() :: #s{}.
-type chan()  :: #chan{}.



%%% Service Interface

-spec join(ChanName, UserName) -> {ok, pid()}
    when ChanName :: string(),
         UserName :: string().

join(ChanName, UserName) ->
    gen_server:call(?MODULE, {join, ChanName, UserName, self()}).


-spec list() -> [{Name :: string(), Desc :: string()}].

list() ->
    gen_server:call(?MODULE, list).


-spec users(ChanName) -> Result
    when ChanName :: string(),
         Result   :: {ok, [UserName :: string()]}
                   | {error, bad_chan}.

users(ChanName) ->
    gen_server:call(?MODULE, {users, ChanName}).


-spec topic(ChanName :: string(), Topic :: string()) -> ok.

topic(ChanName, Topic) ->
    gen_server:cast(?MODULE, {topic, ChanName, Topic}).


%%% gen_server

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {ok, state()}.

init(none) ->
    State = #s{},
    {ok, State}.


handle_call({join, ChanName, UserName, UserPID}, _, State) ->
    {Result, NewState} = do_join(ChanName, UserName, UserPID, State),
    {reply, Result, NewState};
handle_call(list, _, State) ->
    Result = do_list(State),
    {reply, Result, State};
handle_call({users, ChanName}, _, State) ->
    Result = do_users(ChanName, State),
    {reply, Result, State};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp", [From, Unexpected]),
    {noreply, State}.


handle_cast({topic, ChanName, Topic}, State) ->
    NewState = do_topic(ChanName, Topic, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp", [Unexpected]),
    {noreply, State}.


handle_info({'DOWN', Mon, process, PID, Reason}, State) ->
    NewState = handle_down(Mon, PID, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
    {noreply, State}.


handle_down(Mon, PID, Reason, State = #s{chans = Chans}) ->
    case lists:keytake(PID, #chan.pid, Chans) of
        {value, #chan{}, NewChans} ->
            State#s{chans = NewChans};
        false ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected info: ~tp", [Unexpected]),
            State
    end.


code_change(_, State, _) ->
    {ok, State}.


terminate(_, _) ->
    ok.



%%% Doer Functions


do_join(ChanName, UserName, UserPID, State = #s{chans = Chans}) ->
    case lists:keyfind(ChanName, #chan.name, Chans) of
        #chan{pid = ChanPID} ->
            ok = tt_chan:join(ChanPID, UserName, UserPID),
            {{ok, ChanPID}, State};
        false ->
            Result = {ok, ChanPID} = tt_chan:start(ChanName, UserName, UserPID),
            ChanMon = monitor(process, ChanPID),
            Chan = #chan{name = ChanName, pid = ChanPID, mon = ChanMon},
            {Result, State#s{chans = [Chan | Chans]}}
    end.


do_list(#s{chans = Chans}) ->
    [{Name, Topic} || #chan{name = Name, topic = Topic} <- Chans].


do_users(ChanName, #s{chans = Chans}) ->
    case lists:keyfind(ChanName, #chan.name, Chans) of
        #chan{pid = PID} -> {ok, tt_chan:users(PID)};
        false            -> {error, bad_chan}
    end.


do_topic(ChanName, Topic, State = #s{chans = Chans}) ->
    case lists:keyfind(ChanName, #chan.name, Chans) of
        Selected = #chan{} ->
            Updated = Selected#chan{topic = Topic},
            NewChans = lists:keystore(ChanName, #chan.name, Chans, Updated),
            State#s{chans = NewChans};
        false ->
            State
    end.
