%%% @doc
%%% Trash Talk : Chan Worker Supervisor
%%% @end

-module(tt_chan_sup).
-vsn("0.1.0").
-behaviour(supervisor).
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").


-export([start_chan/3]).
-export([start_link/0]).
-export([init/1]).


-spec start_chan(ChanName, OwnerName, OwnerPID) -> Result
    when ChanName  :: string(),
         OwnerName :: string(),
         OwnerPID  :: string(),
         Result    :: {ok, pid()}
                    | {error, Reason},
         Reason    :: {already_started, pid()}
                    | {shutdown, term()}
                    | term().

start_chan(ChanName, OwnerName, OwnerPID) ->
    supervisor:start_child(?MODULE, [ChanName, OwnerName, OwnerPID]).


-spec start_link() -> {ok, pid()}.

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, none).


-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% The OTP init/1 function.

init(none) ->
    RestartStrategy = {simple_one_for_one, 1, 60},
    Chan =
        {tt_chan,
         {tt_chan, start_link, []},
         temporary,
         brutal_kill,
         worker,
         [tt_chan]},
    {ok, {RestartStrategy, [Chan]}}.
