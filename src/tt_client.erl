%%% @doc
%%% Trash Talk Client
%%%
%%% An extremely naive (currently Telnet) client handler.
%%% Unlike other modules that represent discrete processes, this one does not adhere
%%% to any OTP behavior. It does, however, adhere to OTP.
%%%
%%% In some cases it is more comfortable to write socket handlers or a certain
%%% category of state machines as "pure" Erlang processes. This approach is made
%%% OTP-able by use of the proc_lib module, which is the underlying library used
%%% to write the stdlib's behaviors like gen_server, gen_statem, gen_fsm, etc.
%%%
%%% http://erlang.org/doc/design_principles/spec_proc.html
%%% @end

-module(tt_client).
-vsn("0.1.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([chat/4, notice/3, shout/3]).
-export([start/1]).
-export([start_link/1, init/2]).
-export([system_continue/3, system_terminate/4,
         system_get_state/1, system_replace_state/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {name     = {0, []}           :: sized_string(),
         socket   = none              :: none | gen_tcp:socket(),
         size     = {80,24}           :: cell(),
         color    = true              :: boolean(),
         headline = headline()        :: sized_string(),
         nav      = {{1, 2}, { 8,24}} :: panel(),
         frame    = {{9, 2}, {80,22}} :: panel(),
         io       = {{9,24}, {80,24}} :: panel(),
         chans    = []                :: [chan()],
         buff     = {0, []}           :: sized_string(),
         parent   = none              :: none | pid(),
         debug    = []                :: [term()],
         cont     = home              :: atom()}).

-record(chan,
        {name = ""   :: string(),
         pid  = none :: none | pid(),
         mon  = none :: none | reference()}).


%% An alias for the state record above. Aliasing state can smooth out annoyances
%% that can arise from using the record directly as its own type all over the code.

-type state()        :: #s{}.
-type chan()         :: #chan{}.
-type cell()         :: {X :: integer(), Y :: integer()}.
-type panel()        :: {cell(), cell()}.
-type sized_string() :: {Width :: integer(), Text :: string() | binary()}.


%%% Service Interface


-spec chat(Client, ChanName, Sender, Message) -> ok
    when Client   :: pid(),
         ChanName :: string(),
         Sender   :: string(),
         Message  :: string().

chat(Client, ChanName, Sender, Message) ->
    Client ! {chat, ChanName, Sender, Message},
    ok.


-spec notice(Client, ChanName, Message) -> ok
    when Client   :: pid(),
         ChanName :: string(),
         Message  :: string().

notice(Client, ChanName, Message) ->
    Client ! {notice, ChanName, Message},
    ok.


-spec shout(Client, Sender, Message) -> ok
    when Client  :: pid(),
         Sender  :: string(),
         Message :: string().

shout(Client, Sender, Message) ->
    Client ! {shout, Sender, Message},
    ok.


-spec start(ListenSocket) -> Result
    when ListenSocket :: gen_tcp:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: {already_started, pid()}
                       | {shutdown, term()}
                       | term().
%% @private
%% How the tt_client_man or a prior tt_client kicks things off.
%% This is called in the context of tt_client_man or the prior tt_client.

start(ListenSocket) ->
    tt_client_sup:start_acceptor(ListenSocket).


-spec start_link(ListenSocket) -> Result
    when ListenSocket :: gen_tcp:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: {already_started, pid()}
                       | {shutdown, term()}
                       | term().
%% @private
%% This is called by the tt_client_sup. While start/1 is called to iniate a startup
%% (essentially requesting a new worker be started by the supervisor), this is
%% actually called in the context of the supervisor.

start_link(ListenSocket) ->
    proc_lib:start_link(?MODULE, init, [self(), ListenSocket]).


-spec init(Parent, ListenSocket) -> no_return()
    when Parent       :: pid(),
         ListenSocket :: gen_tcp:socket().
%% @private
%% This is the first code executed in the context of the new worker itself.
%% This function does not have any return value, as the startup return is
%% passed back to the supervisor by calling proc_lib:init_ack/2.
%% We see the initial form of the typical arity-3 service loop form here in the
%% call to listen/3.

init(Parent, ListenSocket) ->
    ok = io:format("~p Listening.~n", [self()]),
    Debug = sys:debug_options([]),
    ok = proc_lib:init_ack(Parent, {ok, self()}),
    listen(Parent, Debug, ListenSocket).


-spec listen(Parent, Debug, ListenSocket) -> no_return()
    when Parent       :: pid(),
         Debug        :: [sys:dbg_opt()],
         ListenSocket :: gen_tcp:socket().
%% @private
%% This function waits for a TCP connection. The owner of the socket is still
%% the tt_client_man (so it can still close it on a call to tt_client_man:ignore/0),
%% but the only one calling gen_tcp:accept/1 on it is this process. Closing the socket
%% is one way a manager process can gracefully unblock child workers that are blocking
%% on a network accept.
%%
%% Once it makes a TCP connection it will call start/1 to spawn its successor
%% and send a magic message that will set the client into the proper character mode.
%%
%% The magic codes:                         The magic responses:
%%     IAC  WILL ECHO
%%   <<255, 251, 1>>
%%
%%                                              IAC  DO   ECHO
%%                                            <<255, 253, 1>>
%%
%%     IAC  WILL SUPPRESS_GO_AHEAD
%%   <<255, 251, 3>>
%%
%%                                              IAC  DO   SUPPRESS_GO_AHEAD
%%                                            <<255, 253, 3>>

%%% Unused
%%
%%     IAC  WONT LINEMODE
%%   <<255, 252, 34>>


listen(Parent, Debug, ListenSocket) ->
    case gen_tcp:accept(ListenSocket) of
        {ok, Socket} ->
            {ok, _} = start(ListenSocket),
            {ok, Peer} = zx_net:peername(Socket),
            PeerString = zx_net:host_string(Peer),
            ok = tell(info, "Connection accepted from: ~s", [PeerString]),
            State = #s{socket = Socket, parent = Parent, debug = Debug},
            ok = set_character_mode(Socket),
            NewState = screensize(State),
            greet(NewState);
        {error, closed} ->
            ok = tell(info, "Retiring: Listen socket closed."),
            exit(normal)
     end.


greet(State = #s{socket = Socket}) ->
    ok = gen_tcp:send(Socket, Clear),
    ok = gen_tcp:send(Socket, "Welcome to Trash Talk! What is your username?\r\n"),
    case getline(State) of
        {ok, NextState = #s{buff = Name}} ->
            NewState = screensize(NextState#s{name = Name, buff = <<>>}),
            enroll(NewState);
        {error, unprintable} ->
            ok = forward(Socket, "Wiseguy, eh? Let's try that again..."),
            greet(State)
    end.

enroll(State = #s{name = Name, socket = Socket}) ->
    case tt_client_man:enroll(Name) of
        ok ->
            ok = forward(Socket, ["Nice to meet you, ", Name]),
            home(State#s{name = Name});
        {error, name_taken} ->
            ok = forward(Socket, "Whoops! That name is unavailable. Try again."),
            greet(State)
    end.



%%% Telnet sampling and drawing functions

set_character_mode(Socket) ->
    ok = gen_tcp:send(Socket, <<255, 251, 1>>),
    ok = inet:setopts(Socket, [{active, once}]),
    ok = receive {tcp, Socket, <<255, 253, 1>>} -> ok end,
    ok = gen_tcp:send(Socket, <<255, 251, 3>>),
    ok = inet:setopts(Socket, [{active, once}]),
    ok = receive {tcp, Socket, <<255, 253, 3>>} -> ok end,
    ok.


screensize(State = #s{socket = Socket, size = Size}) ->
    ExtremeCorner = pos(999, 999),
    ok = gen_tcp:send(Socket, [ExtremeCorner, "\r\n"]),
    ok = gen_tcp:send(Socket, "\e[6n"),
    ok = inet:setopts(Socket, [{active, once}]),
    receive {tcp, Socket, Bin = <<"\e[", Rest/binary>>} ->
        case string:split(Rest, ";") of
            [Rows, <<"1R">>] ->
                ok = gen_tcp:send(Socket, ".."),
                Y = binary_to_integer(Rows),
                screensize(State, Y, 3);
            WTF ->
                gen_tcp:send(Socket, "Your terminal is dildos!\r\n")
        end
    end.

screensize(State = #s{socket = Socket}, Y, X) ->
    ok = gen_tcp:send(Socket, ".\e[6n"),
    ok = inet:setopts(Socket, [{active, once}]),
    receive {tcp, Socket, <<"\e[", Rest/binary>>} ->
        case string:split(Rest, ";") of
            [_, <<"2R">>] ->
                Size = {X - 1, Y},
                State#s{size = Size};
            [_, _] ->
                screensize(State, Y, X + 1)
        end
    end.


clear() ->
    "\e[2J".


pos(Col, Row) ->
    X = integer_to_list(Col),
    Y = integer_to_list(Row),
    ["\e[", Y, ";", X, "H"].


draw_io_field(Prompt, Buff, IO = {{X1, Y}, {X2, Y}}) ->
    Width = X2 - X1 - length(Prompt),
    draw_singleline_io(Prompt, Width, Buff);
draw_io_field(Prompt, Buff, IO = {{X1, Y1}, {X2, Y2}}) ->
    Length = X2 - X1,
    Height = Y2 - Y1,
    Prefix = length(Prompt),
    
    draw_io_field(Prompt, Buff, IO, []).

draw_singleline_io(Prompt, Width, Buff) ->
    unicode:characters_to_list(Buff),

draw_io_field(Prompt, Buff, {{


draw_screen(State) ->
    [draw_banner(State),


draw_banner(#s{name = {NW, Name}, headline = {HW, Headline},
               nav = {_, {X2, _}}, size = {Max, _}}) ->
    center(NW, X2, {),


style(true, Strings) ->
    pigment(Strings);
style(false, Strings) ->
    whitewash(Strings).

whitewash([{_, String} | T]) ->
    [String | whitewash(T)];
whitewash([String | T]) ->
    [String, whitewash(T)];
whitewash([]) ->
    [].

pigment([{[], String} | T]) ->
    [String | pigment(T)];
pigment([{Styles, String} | T]) ->
    [["\e[", pimp(Styles), "m", String, "\e[0m"] | pigment(T)];
pigment([String | T]) ->
    [String | pigment(T)];
pigment([]) ->
    [].

pimp([H | T]) ->
    [bling(H), $; | pimp(T)];
pimp([T]) ->
    [bling(T)].

% The infamous ASCII/ANSI effect code table of yore!
% ...well, the best supported bits, anyway.
bling(fg_black)      ->  "30";
bling(fg_red)        ->  "31";
bling(fg_green)      ->  "32";
bling(fg_yellow)     ->  "33";
bling(fg_blue)       ->  "34";
bling(fg_magenta)    ->  "35";
bling(fg_cyan)       ->  "36";
bling(fg_white)      ->  "37";
bling(fg_lt_black)   ->  "90";
bling(fg_lt_red)     ->  "91";
bling(fg_lt_green)   ->  "92";
bling(fg_lt_yellow)  ->  "93";
bling(fg_lt_blue)    ->  "94";
bling(fg_lt_magenta) ->  "95";
bling(fg_lt_cyan)    ->  "96";
bling(fg_lt_white)   ->  "97";
bling(bg_black)      ->  "40";
bling(bg_red)        ->  "41";
bling(bg_green)      ->  "42";
bling(bg_yellow)     ->  "43";
bling(bg_blue)       ->  "44";
bling(bg_magenta)    ->  "45";
bling(bg_cyan)       ->  "46";
bling(bg_white)      ->  "47";
bling(bg_lt_black)   -> "100";
bling(bg_lt_red)     -> "101";
bling(bg_lt_green)   -> "102";
bling(bg_lt_yellow)  -> "103";
bling(bg_lt_blue)    -> "104";
bling(bg_lt_magenta) -> "105";
bling(bg_lt_cyan)    -> "106";
bling(bg_lt_white)   -> "107";
bling(bold)          ->   "1";
bling(underline)     ->   "4";
bling(blink)         ->   "5";
bling(color_swap)    ->   "7";
bling(framed)        ->  "51";
bling(encircled)     ->  "52";
bling(overlined)     ->  "53".



%%% Main loop stuff

home(State) ->
    ok.


getline(State) ->
    getline("> ", State).

getline(Prompt, State = #s{socket = Socket, buff = Buff, io = IO}) ->
    IOField = draw_io_field(Prompt, Buff, IO),
    ok = gen_tcp:send(Socket, IOField),
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<"\r\n">>} ->
            case io_lib:printable_unicode_list(unicode:characters_to_list(Buff)) of
                true  -> {ok, State};
                false -> {error, unprintable}
            end;
        {tcp, Socket, Byte} ->
            NewState = #s{buff = <<Buff/binary, Byte/binary>>},
            getline(Prompt, NewState);
        {chat, ChanName, Sender, Message} ->
            ok = forward(Socket, ["#", ChanName, " - ", Sender, ": ", Message]),
            getline(Prompt, State);
        {notice, ChanName, Message} ->
            ok = forward(Socket, ["#", ChanName, " NOTICE: ", Message]),
            getline(Prompt, State);
        {shout, Sender, Message} ->
            ok = forward(Socket, ["!", Sender, ": ", Message]),
            getline(Prompt, State);
        {tcp_closed, Socket} ->
            exit(normal);
        {'DOWN', Mon, process, PID, Reason} ->
            NewState = handle_down(Mon, PID, Reason, State),
            getline(Prompt, NewState);
        {system, From, Request} ->
            handle_sys(From, Request, State = #s{cont = ?FUNCTION_NAME});
        Unexpected ->
            ok = log(warning, "Unexpected message: ~tp", [Unexpected]),
            getline(Prompt, State)
    end.

getchar(State) ->
    getchar("? ", State).

getchar(Prompt, State = #s{socket = Socket, buff = Buff}) ->
    ok = gen_tcp:send(Socket, Prompt),
    ok = inet:setopts(Socket, [{active, once}]),
    receive
        {tcp, Socket, <<"\r\n">>} ->
            case io_lib:printable_unicode_list(unicode:characters_to_list(Buff)) of
                true  -> {ok, State};
                false -> {error, unprintable}
            end;
        {tcp, Socket, Bytes} ->
            NewState = #s{buff = <<Buff/binary, Bytes/binary>>},
            getchar(Prompt, NewState);
        {chat, ChanName, Sender, Message} ->
            ok = forward(Socket, ["#", ChanName, " - ", Sender, ": ", Message]),
            getchar(Prompt, State);
        {notice, ChanName, Message} ->
            ok = forward(Socket, ["#", ChanName, " NOTICE: ", Message]),
            getchar(Prompt, State);
        {shout, Sender, Message} ->
            ok = forward(Socket, ["!", Sender, ": ", Message]),
            getchar(Prompt, State);
        {tcp_closed, Socket} ->
            exit(normal);
        {'DOWN', Mon, process, PID, Reason} ->
            NewState = handle_down(Mon, PID, Reason, State),
            getchar(Prompt, NewState);
        {system, From, Request} ->
            handle_sys(From, Request, State = #s{cont = ?FUNCTION_NAME});
        Unexpected ->
            ok = log(warning, "Unexpected message: ~tp", [Unexpected]),
            getchar(Prompt, State)
    end.


handle_sys(From, Request, State = #s{parent = Parent, debug = Debug}) ->
    sys:handle_system_msg(Request, From, Parent, ?MODULE, Debug, State).


handle_down(Mon, PID, Reason, State = #s{socket = Socket, chans = Chans}) ->
    case lists:keytake(Mon, #chan.mon, Chans) of
        {value, #chan{name = Name}, NewChans} ->
            ok = forward(Socket, ["-TT: Whoa! #", Name, " just crashed!"]),
            State#s{chans = NewChans};
        false ->
            Unexpected = {'DOWN', Mon, process, PID, Reason},
            ok = log(warning, "Unexpected message: ~tp", [Unexpected]),
            State
    end.


dispatch("#" ++ String, State) ->
    case string:split(String, " ") of
        [ChanName, Message] -> chat(ChanName, Message, State);
        _                   -> derp(State)
    end;
dispatch("!" ++ Message, State) ->
    ok = tt_client_man:shout(Message),
    State;
dispatch("/" ++ Command, State) ->
    case string:split(Command, " ") of
        ["list"]           -> list(State);
        ["users"]          -> users(State);
        ["users", Channel] -> users(Channel, State);
        ["join",  Channel] -> join(Channel, State);
        ["leave", Channel] -> leave(Channel, State);
        ["help", _]        -> help(State);
        ["help"]           -> help(State);
        ["h"]              -> help(State);
        ["?"]              -> help(State);
        ["claim", Channel] -> claim(Channel, State);
        ["topic", String]  -> topic(String, State);
        ["bye"]            -> bye(State);
        _                  -> derp(State)
    end;
dispatch(_, State = #s{socket = Socket}) ->
    ok = forward(Socket, "-TT: Talking to yourself again? Weird..."),
    State.


chat(ChanName, Message, State = #s{socket = Socket, name = Name, chans = Chans}) ->
    ok =
        case lists:keyfind(ChanName, #chan.name, Chans) of
            #chan{pid = ChanPID} ->
                tt_chan:chat(ChanPID, Name, Message);
            false ->
                Message = ["-TT: Whoa there, buddy, you're not in #", ChanName, "!"],
                forward(Socket, Message)
        end,
    State.


list(State = #s{socket = Socket}) ->
    ok =
        case tt_chan_man:list() of
            [] ->
                forward(Socket, "-TT: No channels exist yet");
            Channels ->
                ok = forward(Socket, "-TT: The following channels exist:"),
                Show = fun({C, T}) -> forward(Socket, ["#", C, " - ", T]) end,
                lists:foreach(Show, Channels)
        end,
    State.


users("#" ++ Channel, State = #s{socket = Socket}) ->
    ok =
        case tt_chan_man:users(Channel) of
            {ok, Users} ->
                ok = forward(Socket, ["-TT: Users in #", Channel, ":"]),
                Show = fun(N) -> forward(Socket, N) end,
                lists:foreach(Show, Users);
            {error, bad_chan} ->
                forward(Socket, "-TT: Channel does not exist.")
        end,
    State;
users(_, State) ->
    derp(State).


users(State = #s{socket = Socket}) ->
    Names = tt_client_man:users(),
    Message =
        ["-TT: USER LIST\r\n"
         "-------------------------------------------------------------------\r\n",
         [[Name, "\r\n"] || Name <- Names],
         "-------------------------------------------------------------------"],
    ok = forward(Socket, Message),
    State.


join("#" ++ Channel, State = #s{socket = Socket, name = Name, chans = Chans}) ->
    case lists:keymember(Channel, #chan.name, Chans) of
        false ->
            {ok, ChanPID} = tt_chan_man:join(Channel, Name),
            ChanMon = monitor(process, ChanPID),
            Chan = #chan{name = Channel, pid = ChanPID, mon = ChanMon},
            ok = forward(Socket, ["-TT: Joined #", Channel]),
            State#s{chans = [Chan | Chans]};
        true ->
            ok = forward(Socket, ["-TT: Already in #", Channel, "!"]),
            State
    end;
join(_, State) ->
    derp(State).


leave("#" ++ Channel, State = #s{socket = Socket, chans = Chans}) ->
    case lists:keytake(Channel, #chan.name, Chans) of
        {value, #chan{pid = PID, mon = Mon}, NewChans} ->
            true = demonitor(Mon, [flush]),
            ok = tt_chan:leave(PID),
            ok = forward(Socket, ["-TT: Left ", Channel]),
            State#s{chans = NewChans};
        false ->
            Message = ["-TT: Slow down, turbo, you're not even in #", Channel, "!"],
            ok = forward(Socket, Message),
            State
    end;
leave(_, State) ->
    derp(State).


help(State = #s{socket = Socket}) ->
    Message =
        "-TT: HELP PAGE\r\n"
        "-------------------------------------------------------------------\r\n"
        "-TT: Available commands:\r\n"
        "-TT: ![message]\r\n"
        "-TT:     Global shout (everyone can see this)\r\n"
        "-TT: \r\n"
        "-TT: #[channel name] [message]\r\n"
        "-TT:     Channel message (everyone in the channel will see it)\r\n"
        "-TT: \r\n"
        "-TT: /list\r\n"
        "-TT:     Lists available channels\r\n"
        "-TT: \r\n"
        "-TT: /users #[channel name]\r\n"
        "-TT:     Show list of users in the given channel\r\n"
        "-TT: \r\n"
        "-TT: /users\r\n"
        "-TT:     Show all current users\r\n"
        "-TT: \r\n"
        "-TT: /join #[channel name]\r\n"
        "-TT:     Joins a channel, creating it if new.\r\n"
        "-TT: \r\n"
        "-TT: /leave #[channel name]\r\n"
        "-TT:     Leaves a channel, dropping the channel if you're last.\r\n"
        "-TT: \r\n"
        "-TT: /help, /h, /?\r\n"
        "-TT:     Show this help page.\r\n"
        "-TT: \r\n"
        "-TT: /claim #[channel name]\r\n"
        "-TT:     Claim ownership of a channel\r\n"
        "-TT: \r\n"
        "-TT: /topic #[channel name] [topic text]\r\n"
        "-TT:     Set the topic of a channel if you are the owner.\r\n"
        "-TT: \r\n"
        "-TT: /bye\r\n"
        "-TT:     Obvious\r\n"
        "-------------------------------------------------------------------",
    ok = forward(Socket, Message),
    State.


claim("#" ++ Channel, State = #s{socket = Socket, chans = Chans}) ->
    case lists:keyfind(Channel, #chan.name, Chans) of
        #chan{pid = PID} ->
            claim2(PID, State);
        false ->
            ok = forward(Socket, "-TT: Derp! You're not even in that channel!"),
            State
    end.

claim2(ChanPID, State = #s{socket = Socket, chans = Chans}) ->
    case tt_chan:claim(ChanPID) of
        ok ->
            State;
        {error, owned} ->
            ok = forward(Socket, "-TT: That channel is already owned!"),
            State;
        {error, bad_user} ->
            ok = forward(Socket, "-TT: Derp! You're not even in that channel!"),
            NewChans = lists:keydelete(ChanPID, #chan.pid, Chans),
            State#s{chans = NewChans}
    end.


topic(String, State) ->
    case string:split(String, " ") of
        ["#" ++ ChanName, Topic] -> topic2(ChanName, Topic, State);
        ["#" ++ ChanName]        -> show_topic(ChanName, State);
        _                        -> derp(State)
    end.

topic2(ChanName, Topic, State = #s{socket = Socket, chans = Chans}) ->
    case lists:keyfind(ChanName, #chan.name, Chans) of
        #chan{pid = PID} ->
            topic3(PID, Topic, State);
        false ->
            ok = forward(Socket, "-TT: Derp! You're not even in that channel!"),
            State
    end.

topic3(ChanPID, Topic, State = #s{socket = Socket}) ->
    ok =
        case tt_chan:topic(ChanPID, Topic) of
            ok                 -> ok;
            {error, not_owner} -> forward(Socket, "-TT: You don't own that channel!")
        end,
    State.

show_topic(ChanName, State = #s{socket = Socket, chans = Chans}) ->
    ok =
        case lists:keyfind(ChanName, #chan.name, Chans) of
            #chan{pid = PID} ->
                Message = ["#", ChanName, " - ", tt_chan:topic(PID)],
                forward(Socket, Message);
            false ->
                forward(Socket, "-TT: Derp! You're not even in that channel!")
        end,
    State.


bye(#s{socket = Socket}) ->
    ok = forward(Socket, "-TT: Bye!"),
    ok = gen_tcp:shutdown(Socket, read_write),
    exit(normal).


derp(State = #s{socket = Socket}) ->
    ok = forward(Socket, "-TT: Arglebargle, glop-glyf!?!"),
    State.


forward(Socket, Message) ->
    gen_tcp:send(Socket, Message).


headline() ->
    Text = "Trash Talk!",
    {length(Text), Text}.


-spec system_continue(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
%% @private
%% The function called by the OTP internal functions after a system message has been
%% handled. If the worker process has several possible states this is one place
%% resumption of a specific state can be specified and dispatched.

system_continue(Parent, Debug, State = #s{cont = Cont}) ->
    Cont(State).


-spec system_terminate(Reason, Parent, Debug, State) -> no_return()
    when Reason :: term(),
         Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
%% @private
%% Called by the OTP inner bits to allow the process to terminate gracefully.
%% Exactly when and if this is callback gets called is specified in the docs:
%% See: http://erlang.org/doc/design_principles/spec_proc.html#msg

system_terminate(Reason, _Parent, _Debug, _State) ->
    exit(Reason).



-spec system_get_state(State) -> {ok, State}
    when State :: state().
%% @private
%% This function allows the runtime (or anything else) to inspect the running state
%% of the worker process at any arbitrary time.

system_get_state(State) -> {ok, State}.


-spec system_replace_state(StateFun, State) -> {ok, NewState, State}
    when StateFun :: fun(),
         State    :: state(),
         NewState :: term().
%% @private
%% This function allows the system to update the process state in-place. This is most
%% useful for state transitions between code types, like when performing a hot update
%% (very cool, but sort of hard) or hot patching a running system (living on the edge!).

system_replace_state(StateFun, State) ->
    {ok, StateFun(State), State}.
